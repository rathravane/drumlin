package com.rathravane.drumlin.app.userAgents.browsers;

public interface browser
{
	String getName ();
	String getVersion ();
}
