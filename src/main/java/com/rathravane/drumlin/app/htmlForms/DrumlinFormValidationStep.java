/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.app.htmlForms;

import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper.ParseException;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

/**
 * A form validation step.
 * @author peter@rathravane.com
 *
 */
public interface DrumlinFormValidationStep
{
	/**
	 * <p>Given a context, form, and field information, decide if the value is valid. If it is not,
	 * add an error listing for the field to the supplied validation error. (Once all
	 * validation steps are complete, if the error object contains any errors, it's thrown as
	 * an exception, indicating a problem with the form submission.)</p>
	 * 
	 * <p>For form-level validation, the field argument is null.</p>
	 * 
	 * @param context
	 * @param form
	 * @param field Field info, or null for form-level validation.
	 * @param err
	 * @throws ParseException 
	 */
	void validate ( DrumlinRequestContext context, DrumlinFormPostWrapper form, DrumlinFormFieldInfo field, DrumlinInvalidFormException err ) throws ParseException;
}
