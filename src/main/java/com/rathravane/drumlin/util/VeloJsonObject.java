/*
 *	Copyright 2006-2014, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.util;

import java.util.Collection;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VeloJsonObject
{
	public VeloJsonObject ( JSONObject o )
	{
		fObject = o;
	}

	public Object get ( String key )
	{
		try
		{
			final Object o = fObject.get ( key );
			if ( o instanceof JSONObject )
			{
				return new VeloJsonObject ( (JSONObject) o ); 
			}
			else if ( o != null )
			{
				return o.toString ();
			}
		}
		catch ( JSONException e )
		{
			log.info ( e.getMessage(), e );
		}
		return null;
	}

	public String getString ( String key, String defValue )
	{
		return fObject.optString ( key, defValue );
	}

	public boolean hasValueFor ( String key )
	{
		return fObject.has ( key );
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getAllKeys ()
	{
		return fObject.keySet ();
	}

	private final JSONObject fObject;
	private static final Logger log = LoggerFactory.getLogger ( VeloJsonObject.class );
}
