/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.playish;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;

import com.rathravane.drumlin.service.framework.DrumlinServlet;
import com.rathravane.drumlin.service.framework.routing.playish.DrumlinPlayishRoutingFileSource;

public class PlayishServlet extends DrumlinServlet
{
	public PlayishServlet ()
	{
	}

	@Override
	protected void servletSetup () throws ServletException
	{
		try
		{
			final String routerConfigFilename = getSettings().getString ( kRouterConfigFile, "WEB-INF/routing.txt" );
			final URL routingFile = findStream ( routerConfigFilename );
			getRequestRouter ().addRouteSource ( new DrumlinPlayishRoutingFileSource ( routingFile ) );
		}
		catch ( IOException e )
		{
			throw new ServletException ( e );
		}
	}

	private static final String kRouterConfigFile = "drumlin.router.config";
	private static final long serialVersionUID = 1L;
}
