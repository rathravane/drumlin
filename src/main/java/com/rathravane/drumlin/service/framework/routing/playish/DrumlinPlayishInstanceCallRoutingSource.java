/*
 *	Copyright 2006-2016, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.routing.playish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.slf4j.LoggerFactory;

import com.rathravane.drumlin.service.framework.DrumlinConnection;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.framework.routing.DrumlinRouteInvocation;
import com.rathravane.drumlin.service.framework.routing.DrumlinRouteSource;

/**
 * This routing source routes to methods on an object instance.
 * 
 * @author peter@rathravane.com
 *
 */
public class DrumlinPlayishInstanceCallRoutingSource<T> implements DrumlinRouteSource
{
	public DrumlinPlayishInstanceCallRoutingSource ( T instance, URL url ) throws IOException
	{
		fInstance = instance;
		fPathList = new LinkedList<DrumlinPathInfo> ();
		fPackages = new LinkedList<String> ();

		if ( url == null )
		{
			throw new IOException ( "URL for routing file is null in drumlinPlayStyleRoutingFileSource ( URL u )" );
		}
		loadRoutes ( url );
	}

	public DrumlinPlayishInstanceCallRoutingSource ( T instance, InputStream is ) throws IOException
	{
		fInstance = instance;
		fPathList = new LinkedList<DrumlinPathInfo> ();
		fPackages = new LinkedList<String> ();

		loadRoutes ( is );
	}

	/**
	 * Add a verb and path route with an action string. The action can start with "staticDir:" or
	 * "staticFile:". The remainder of the string is used as a relative filename to the dir (staticDir:), or 
	 * as a filename (staticFile:).
	 * @param verb
	 * @param path
	 * @param action
	 * @return this object (for use in chaining the add calls)
	 */
	public synchronized DrumlinPlayishInstanceCallRoutingSource<T> addRoute ( String verb, String path, String action )
	{
		final DrumlinPathInfo pe = DrumlinPathInfo.processPath ( verb, path );
		pe.setHandler ( new InstanceEntryAction<T> ( fInstance, action, pe.getArgs(), fPackages ) );
		fPathList.add ( pe );

		return this;
	}

	/**
	 * Get a route invocation for a given verb+path, or null.
	 */
	@Override
	public synchronized DrumlinRouteInvocation getRouteFor ( String verb, String path, DrumlinConnection forSession )
	{
		DrumlinRouteInvocation selected = null;
		for ( DrumlinPathInfo pe : fPathList )
		{
			final List<String> args = pe.matches ( verb, path );
			if ( args != null )
			{
				selected = getInvocation ( pe, args );
				break;
			}
		}
		return selected;
	}

	/**
	 * Get the URL that reaches a given static method with the given arguments. 
	 */
	@Override
	public String getRouteTo ( Class<?> c, String staticMethodName, Map<String, Object> args, DrumlinConnection forSession )
	{
		final String fullname = c.getName() + "." + staticMethodName;
		for ( DrumlinPathInfo pe : fPathList )
		{
			if ( pe.invokes ( fullname ) )
			{
				return pe.makePath ( args );
			}
		}
		return null;
	}

	private final T fInstance;
	private final LinkedList<String> fPackages;
	private final LinkedList<DrumlinPathInfo> fPathList;

	private static final org.slf4j.Logger log = LoggerFactory.getLogger ( DrumlinPlayishInstanceCallRoutingSource.class );

	protected Invocation getInvocation ( DrumlinPathInfo pe, List<String> args )
	{
		return new Invocation ( pe, args );
	}

	protected class Invocation implements DrumlinRouteInvocation
	{
		public Invocation ( DrumlinPathInfo pe, List<String> args )
		{
			fPe = pe;
			fArgs = args;
		}

		@Override
		public void run ( DrumlinRequestContext ctx ) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
		{
			fPe.getHandler ().handle ( ctx, fArgs );
		}
	
		private final DrumlinPathInfo fPe;
		private final List<String> fArgs;
	}

	protected synchronized void clearRoutes ()
	{
		log.debug ( "Clearing routes within this instance route source." );
		fPathList.clear ();
	}

	protected synchronized void addPackage ( String pkg )
	{
		fPackages.add ( pkg );
	}


	private synchronized void loadRoutes ( URL u ) throws IOException
	{
		loadRoutes ( new InputStreamReader ( u.openStream () ) );
	}

	private synchronized void loadRoutes ( InputStream is ) throws IOException
	{
		loadRoutes ( new InputStreamReader ( is ) );
	}

	private synchronized void loadRoutes ( Reader r ) throws IOException
	{
		clearRoutes ();

		final BufferedReader fr = new BufferedReader ( r );
		
		String line;
		while ( ( line = fr.readLine () ) != null )
		{
			line = line.trim ();
			if ( line.length () > 0 && !line.startsWith ( "#" ) )
			{
				processLine ( line );
			}
		}
	}

	private void processLine ( String line )
	{
		try
		{
			final StringTokenizer st = new StringTokenizer ( line );
			final String verb = st.nextToken ();
			if ( verb.toLowerCase ().equals ( "package" ) )
			{
				final String pkg = st.nextToken ();
				addPackage ( pkg );
			}
			else
			{
				final String path = st.nextToken ();
				final String action = st.nextToken ();
				addRoute ( verb, path, action );
			}
		}
		catch ( NoSuchElementException e )
		{
			log.warn ( "There was an error processing route config line: \"" + line + "\"" );
		}
		catch ( IllegalArgumentException e )
		{
			log.warn ( "There was an error processing route config line: \"" + line + "\": " + e.getMessage () );
		}
	}
}
