package com.rathravane.drumlin.service.framework.context;

import javax.servlet.http.HttpServletRequest;

import junit.framework.TestCase;

import org.junit.Test;

public class StdRequestTest extends TestCase
{
	@Test
	public void testPath ()
	{
		final HttpServletRequest base = new MockServletRequest ()
		{
			@Override
			public String getPathInfo () { return "foo/bar/bee"; }

			@Override
			public String getContextPath () { return "/context"; }

			@Override
			public String getRequestURI () { return "/context/foo/bar%2Fbee"; }
		};

		final StdRequest req = new StdRequest ( base );
		final String pic = req.getPathInContext ();
		assertEquals ( "/foo/bar%2Fbee", pic );
	}
}
