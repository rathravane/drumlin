/*
 *	Copyright 2006-2013, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.staticFiles;

import com.rathravane.drumlin.service.standalone.DrumlinStandaloneServer;
import com.rathravane.till.logging.rrLogSetup;

public class StaticFileServer extends DrumlinStandaloneServer
{
	public static void main ( String[] args )
	{
		try
		{
			final StaticFileServer sfs = new StaticFileServer ();
			sfs.runFromMain ( args );
		}
		catch ( Exception e )
		{
			log.error ( "caught exception in main()", e );
			System.exit ( -1 );
		}
	}

	private static final org.slf4j.Logger log = rrLogSetup.getLog ( StaticFileServer.class );
}
