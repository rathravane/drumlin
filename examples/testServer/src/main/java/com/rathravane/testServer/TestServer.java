package com.rathravane.testServer;

import javax.servlet.Servlet;

import com.rathravane.drumlin.service.standalone.DrumlinStandaloneServer;
import com.rathravane.till.nv.rrNvReadable;

public class TestServer extends DrumlinStandaloneServer
{
	public TestServer ()
	{
		super ( "Test Server", "testServer" );
	}

	@Override
	protected Servlet createServlet ( rrNvReadable p )
	{
		return new TestServlet ( p );
	}

	public static void main ( String[] args )
	{
		try
		{
			new TestServer ().runFromMain ( args );
		}
		catch ( Exception e )
		{
			System.err.println ( e.getMessage () );
			e.printStackTrace ( System.err );
		}
	}
}
