/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.app.htmlFoms;

import com.rathravane.drumlin.service.framework.DrumlinConnection;
import com.rathravane.drumlin.service.framework.DrumlinServlet;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

public class mockHandlingContext<C extends DrumlinConnection> extends DrumlinRequestContext
{
	public mockHandlingContext ()
	{
		super ( new mockWebServlet (), null, null, null, null, null );
	}
	
	private static class mockWebServlet extends DrumlinServlet
	{
		public mockWebServlet ()
		{
		}
		private static final long serialVersionUID = 1L;
	}
}
