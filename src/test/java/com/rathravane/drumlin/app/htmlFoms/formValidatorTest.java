/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.app.htmlFoms;

import java.util.HashMap;

import junit.framework.TestCase;

import org.junit.Test;

import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper;
import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper.ParseException;
import com.rathravane.drumlin.app.htmlForms.DrumlinFormValidator;
import com.rathravane.drumlin.app.htmlForms.DrumlinInvalidFormException;
import com.rathravane.drumlin.service.framework.DrumlinConnection;

public class formValidatorTest<C extends DrumlinConnection> extends TestCase
{
	@Test
	public void testRequiredField () throws DrumlinInvalidFormException, ParseException
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).matches ( ".*", "anything" ).defaultValue ( "hiya" );

		final HashMap<String,String[]> map = new HashMap<>();
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );
		final mockHandlingContext<C> ctx = new mockHandlingContext<>();
		v.validate ( ctx, w );

		assertEquals ( "hiya", w.getValue ( "test" ) );
	}

	@Test
	public void testValidation () throws DrumlinInvalidFormException
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).required ("").matches ( ".*", "anything" ).defaultValue ( "hiya" );

		final HashMap<String,String[]> map = new HashMap<>();
		map.put ( "test", new String[] { "fubar" } );
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );

		final mockHandlingContext<C> ctx = new mockHandlingContext<>();
		v.validate ( ctx, w );
	}

	@Test
	public void testMissingField ()
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).required ("");

		final HashMap<String,String[]> map = new HashMap<>();
		map.put ( "test", new String[] { "" } );
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );

		try
		{
			final mockHandlingContext<C> ctx = new mockHandlingContext<>();
			v.validate ( ctx, w );
			fail ( "Reqd field is missing" );
		}
		catch ( DrumlinInvalidFormException e )
		{
			/* expected exception */
		}
	}

	@Test
	public void testMissingFieldWithDefault ()
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).defaultValue ( "hiya" );

		final HashMap<String,String[]> map = new HashMap<>();
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );

		try
		{
			final mockHandlingContext<C> ctx = new mockHandlingContext<>();
			v.validate ( ctx, w );
		}
		catch ( DrumlinInvalidFormException e )
		{
			fail ( "Reqd field is missing but has default" );
		}
	}

	@Test
	public void testRegexMatches ()
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).matches ( "bar|bee", "Test must be bar or bee." );

		final HashMap<String,String[]> map = new HashMap<>();
		map.put ( "test", new String[] { "bar" } );
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );

		try
		{
			final mockHandlingContext<C> ctx = new mockHandlingContext<>();
			v.validate ( ctx, w );
		}
		catch ( DrumlinInvalidFormException e )
		{
			fail ( "regex matches" );
		}
	}

	@Test
	public void testRegexDoesntMatch ()
	{
		final DrumlinFormValidator v = new DrumlinFormValidator ();
		v.field ( "test" ).matches ( "bar|bee", "Test must be bar or bee." );

		final HashMap<String,String[]> map = new HashMap<>();
		map.put ( "test", new String[] { "" } );
		final DrumlinFormPostWrapper w = new DrumlinFormPostWrapper ( new mockRequest ( map ) );

		try
		{
			final mockHandlingContext<C> ctx = new mockHandlingContext<>();
			v.validate ( ctx, w );
			fail ( "field doesn't match" );
		}
		catch ( DrumlinInvalidFormException e )
		{
			assertEquals ( "Test must be bar or bee.", e.getFieldProblems ().get ( "test" ).iterator ().next () );
		}
	}
}
