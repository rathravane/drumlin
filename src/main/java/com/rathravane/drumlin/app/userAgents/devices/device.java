package com.rathravane.drumlin.app.userAgents.devices;

public interface device
{
	String getName ();
	String getVersion ();

	screenInfo getScreenInfo ();

	String getOsName ();
	String getOsVersion ();

	boolean isMobile ();
}
