/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.rendering;

/**
 * An interface to the Velocity renderer. Get/put/remove objects
 * for use in VTL; Render templates to the response stream (provided
 * by the handlingContext where the renderContext was acquired).
 * 
 * @author peter
 */
public interface DrumlinRenderContext
{
	/**
	 * Get an object in the context by name.
	 * @param key
	 * @return an object, or null.
	 */
	Object get ( String key );

	/**
	 * Put an object into the render context with a name. The name is available
	 * in Velocity VTL.
	 * 
	 * @param key
	 * @param o
	 */
	DrumlinRenderContext put ( String key, Object o );

	/**
	 * Remove an object given its name.
	 * @param key
	 */
	DrumlinRenderContext remove ( String key );

	/**
	 * Render the named template.
	 * @param templateName
	 */
	void renderTemplate ( String templateName );

	/**
	 * Render the named template with the given content type in the HTTP header.
	 * @param templateName
	 * @param contentType
	 */
	void renderTemplate ( String templateName, String contentType );
}
