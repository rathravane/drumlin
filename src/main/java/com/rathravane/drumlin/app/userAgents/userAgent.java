package com.rathravane.drumlin.app.userAgents;

public interface userAgent
{
	String getDeviceName ();
	String getOsName ();
	String getOsVersion ();

	String getBrowserCanonicalName ();
	String getBrowserCanonicalVersion ();

	boolean getIsMobile ();

	boolean getIsFixedScreenSize ();
	int getScreenWidth ();
	int getScreenHeight ();
	int getScreenDpi ();
}
