/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.routing;

import java.util.Map;

import com.rathravane.drumlin.service.framework.DrumlinConnection;

/**
 * A route source is a collection of routes that are requested by verb (e.g. GET) and
 * a path. A Drumlin app can have any number of route sources. During request handling,
 * each route source is tested in order via getRouteFor(). If the route source returns
 * a {@link DrumlinRouteInvocation}, it's used to handle the request.
 * 
 * @author peter@rathravane.com
 *
 */
public interface DrumlinRouteSource
{
	/**
	 * Return the route handler for a given verb and path or null.
	 * @param verb
	 * @param path
	 * @return
	 */
	DrumlinRouteInvocation getRouteFor ( String verb, String path, DrumlinConnection forSession );

	/**
	 * Code in this system can create a URL to get to a specific class + method by asking
	 * the router to find a reverse-route. If this route source has routes that point to
	 * static entry points, it should implement an override that returns the correct URL.
	 * 
	 * @param c
	 * @param staticMethodName
	 * @param args
	 * @param forSession, which can be null
	 * @return null, or a URL to get to the entry point
	 */
	String getRouteTo ( Class<?> c, String staticMethodName, Map<String, Object> args, DrumlinConnection forSession );
}
