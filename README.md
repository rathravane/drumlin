Drumlin is a small Java webapp framework
=======

Drumlin provides two basic features:

1. Map inbound URL paths to code entry points
2. Render POJOs into templates (normally HTML)

You can build request routers in a bunch of ways by implementing a particular Router interface,
the library includes a [Play](http://www.playframework.org)-style routing file reader. We use that
most often.

For object rendering, we include a dependency on [Apache Velocity](http://velocity.apache.org/).

Build with Maven. For example:

	mvn clean package

