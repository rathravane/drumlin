/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.routing;

import java.io.IOException;
import java.util.Map;

import com.rathravane.drumlin.service.framework.DrumlinConnection;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

/**
 * A basic route handler provided for convenience in creating simple handlers.
 * 
 * @author peter@rathravane.com
 */
public abstract class DrumlinSimpleRouteHandler<C extends DrumlinConnection> implements DrumlinRouteSource, DrumlinRouteInvocation
{
	public DrumlinRouteInvocation getRouteFor ( String verb, String path, DrumlinConnection forSession )
	{
		return this;
	}

	public String getRouteTo ( Class<?> c, String staticMethodName, Map<String, Object> args, DrumlinConnection forSession )
	{
		return null;
	}

	public abstract void run ( DrumlinRequestContext ctx ) throws IOException;
}
