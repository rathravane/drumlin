package com.rathravane.drumlin.monitors;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Layout;
import ch.qos.logback.core.LayoutBase;
import ch.qos.logback.core.UnsynchronizedAppenderBase;

import com.rathravane.till.data.rrStreamTools;

/**
 * 
 */
public class LogbackToSlack<E extends ILoggingEvent> extends UnsynchronizedAppenderBase<E>
{
	public void setToken ( String token )
	{
		fToken = token;
	}

	public void setChannel ( String channel )
	{
		fChannel = channel;
	}

	public void setUser ( String user )
	{
		fUser = user;
	}

	public void setLayout ( final Layout<E> layout )
	{
		fLayout = layout;
	}

	@Override
	protected void append ( final E event )
	{
		try
		{
			// build the post body
			final StringWriter sw = new StringWriter ();
			sw
				.append ( "token=" )
				.append ( URLEncoder.encode ( fToken, "UTF-8" ) )
				.append ( "&channel=" )
				.append ( URLEncoder.encode ( fChannel, "UTF-8" ) )
				.append ( "&username=" )
				.append ( URLEncoder.encode ( fUser, "UTF-8" ) )
				.append ( "&text=" )
				.append ( URLEncoder.encode ( makeMessage ( event ), "UTF-8" ) )
			;
			final String attachments = makeAttachments ( event );
			if ( attachments != null )
			{
				sw
					.append ( "&attachments=" )
					.append ( URLEncoder.encode ( makeAttachments ( event ), "UTF-8" ) )
				;
			}
			final byte[] postBodyBytes = sw.toString ().getBytes ( "UTF-8" );

			// post it to slack
			final URL url = new URL ( "https://slack.com/api/chat.postMessage" );
			final HttpURLConnection conn = (HttpURLConnection) url.openConnection ();
			conn.setDoOutput ( true );
			conn.setRequestMethod ( "POST" );
			conn.setFixedLengthStreamingMode ( postBodyBytes.length );
			conn.setRequestProperty ( "Content-Type", "application/x-www-form-urlencoded" );
			rrStreamTools.copyStream ( new ByteArrayInputStream ( postBodyBytes ), conn.getOutputStream () );

			final int status = conn.getResponseCode ();
			final String msg = conn.getResponseMessage ();
			if ( status > 299 )
			{
				addError ( "Can't post to Slack: " + status + " " + msg );
			}
		}
		catch ( IOException e )
		{
			addError ( "Can't post to Slack: " + e.getMessage (), e );
		}
	}

	private String fToken;
	private String fChannel = "#random";
	private String fUser = "drumlin";
	private Layout<E> fLayout = new LayoutBase<E>()
	{
        @Override
		public String doLayout ( E event )
        {
        	return new StringWriter ()
        		.append ( event.getLevel ().toString () )
        		.append ( " " )
        		.append ( event.getLoggerName () )
        		.append ( ": " )
        		.append ( event.getFormattedMessage ().replaceAll ( "\n", "\n\t" ) )
        		.toString ()
        	;
        }
    };

	private String makeMessage ( final E evt )
	{
		return fLayout.doLayout ( evt );
	}
	
	private String makeAttachments ( final E evt )
	{
		return null;
	}
}
