/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.routing.playish;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

/**
 * A route handler handles a request, given a context. 
 * @author peter
 */
public interface DrumlinPlayishRouteHandler
{
	void handle ( DrumlinRequestContext context, List<String> args ) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	boolean actionMatches ( String fullPath );
}
