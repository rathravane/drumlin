/*
 *	Copyright 2006-2016, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.app.htmlForms;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.rathravane.till.collections.rrMultiMap;

/**
 * An exception signaling that a form is invalid. It carries specific problems
 * (and warnings) by field name, and separately for the overall form. It's intended
 * to be caught and used to report problems back to the user.
 * 
 * @author peter@rathravane.com
 */
public class DrumlinInvalidFormException extends Exception
{
    public DrumlinInvalidFormException ()
	{
		super ( "Form validation errors" );
		fFieldProblems = new rrMultiMap<>();
		fFieldWarnings = new rrMultiMap<>();
		fFormProblems = new LinkedList<>();
	}

	/**
	 * Add a general problem.
	 * @param problem
	 */
	public DrumlinInvalidFormException addProblem ( String problem )
	{
		fFormProblems.add ( problem );
		return this;
	}

	/**
	 * Add a problem with a specific field.
	 * @param field
	 * @param problem
	 */
	public DrumlinInvalidFormException addProblem ( String field, String problem )
	{
		fFieldProblems.put ( field, problem );
		return this;
	}

	/**
	 * Add a warning for a specific field.
	 * @param field
	 * @param problem
	 */
	public DrumlinInvalidFormException addWarning ( String field, String problem )
	{
		fFieldWarnings.put ( field, problem );
		return this;
	}

	/**
	 * Copy problems from another invalid form exception.
	 * @param that
	 */
	public DrumlinInvalidFormException addProblemsFrom ( DrumlinInvalidFormException that )
	{
		fFormProblems.addAll ( that.getFormProblems () );
		fFieldProblems.putAll ( that.getFieldProblems () );
		return this;
	}

	/**
	 * Get the count of problems.
	 * @return the count of problems.
	 */
	public int size ()
	{
		return fFieldProblems.size () + fFormProblems.size ();
	}

	/**
	 * Get all field problems.
	 * @return a map from field name to a list of problem strings
	 */
	public Map<String,List<String>> getFieldProblems ()
	{
		return fFieldProblems.getValues ();
	}

	/**
	 * Get problems on a particular field.
	 * @param field
	 * @return a list of 0 or more problems.
	 */
    public List<String> getProblemsOn ( String field )
	{
		final LinkedList<String> list = new LinkedList<>();
		final List<String> vals = fFieldProblems.get ( field );
		if ( vals != null )
		{
			list.addAll ( vals );
		}
		return list;
	}
	
	/**
	 * Get all field warnings.
	 * @return a map from field name to a list of warning strings
	 */
	public Map<String,List<String>> getFieldWarnings ()
	{
		return fFieldWarnings.getValues ();
	}

	/**
	 * Get the form-level problems.
	 * @return a list of form problems
	 */
	public List<String> getFormProblems ()
	{
		return fFormProblems;
	}

	private final LinkedList<String> fFormProblems; 
	private rrMultiMap<String,String> fFieldProblems;
	private rrMultiMap<String,String> fFieldWarnings;
	private static final long serialVersionUID = 1L;
}
