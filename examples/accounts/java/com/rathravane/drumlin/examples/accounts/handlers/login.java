package com.rathravane.drumlin.examples.accounts.handlers;

import java.io.IOException;

import com.rathravane.drumlin.app.accounts.DrumlinAccountMgr;
import com.rathravane.drumlin.app.accounts.DrumlinAccountsException;
import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper;
import com.rathravane.drumlin.examples.accounts.exampleAccount;
import com.rathravane.drumlin.examples.accounts.exampleUser;
import com.rathravane.drumlin.examples.accounts.loginServlet;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

public class login
{
	public static void get ( DrumlinRequestContext context ) throws IOException
	{
		context.renderer ().renderTemplate ( "login.html" );
	}

	private static final String kSameAcct = "everyoneInTheSameAccount";

	public static void post ( DrumlinRequestContext context ) throws IOException
	{
		final DrumlinAccountMgr<exampleUser,exampleAccount> am = ((loginServlet)(context.getServlet ())).getAccountMgr ();

		final DrumlinFormPostWrapper wrapper = new DrumlinFormPostWrapper ( context.request () );
		final String username = wrapper.getValue ( "username", null );
		if ( username != null )
		{
			context.renderer ().put ( "lastUsername", username );
		}

		if ( wrapper.hasParameter ( "create" ))
		{
			try
			{
				if ( !am.accountExists ( kSameAcct ) )
				{
					am.createAccount ( kSameAcct );
					am.enableAccount ( kSameAcct, true );
				}

				final exampleUser u = am.createUser ( kSameAcct, username, wrapper.getValue("username") );
				if ( u != null )
				{
					am.setPassword ( username, wrapper.getValue("password") );
					context.renderer ().put ( "createResult", "created" );
				}
				else
				{
					context.renderer ().put ( "createResult", "invalid" );
				}
			}
			catch ( DrumlinAccountsException e )
			{
				context.renderer ().put ( "authResult", "error: " + e.getMessage() );
			}
		}
		else if ( wrapper.hasParameter ( "enable" ))
		{
			try
			{
				am.enableUser ( username, true );
				context.renderer ().put ( "enableResult", "ok" );
			}
			catch ( DrumlinAccountsException e )
			{
				context.renderer ().put ( "enableResult", "error: " + e.getMessage() );
			}
		}
		else if ( wrapper.hasParameter ( "disable" ))
		{
			try
			{
				am.enableUser ( username, false );
				context.renderer ().put ( "enableResult", "ok" );
			}
			catch ( DrumlinAccountsException e )
			{
				context.renderer ().put ( "enableResult", "error: " + e.getMessage() );
			}
		}
		else if ( wrapper.hasParameter ( "login" ))
		{
			// handle login
			try
			{
				final exampleUser u = am.authenticateUserWithPassword ( username, wrapper.getValue ( "password" ) );
				if ( u != null )
				{
					context.renderer ().put ( "authResult", "authenticated" );
				}
				else
				{
					context.renderer ().put ( "authResult", "invalid" );
				}
			}
			catch ( DrumlinAccountsException e )
			{
				context.renderer ().put ( "authResult", "error: " + e.getMessage() );
			}
		}
		else if ( wrapper.hasParameter ( "pwreset" ))
		{
			try
			{
				final String tag = am.requestPasswordReset ( username, 30, "some nonsense" );
				if ( tag != null )
				{
					context.renderer ().put ( "resetResult", "tag [" + tag + "] expires in 30 seconds" );
					context.renderer ().put ( "resetUrl", "reset/" + tag );
				}
				else
				{
					context.renderer ().put ( "resetResult", "couldn't reset" );
				}
			}
			catch ( DrumlinAccountsException e )
			{
				context.renderer ().put ( "resetResult", "error: " + e.getMessage() );
			}
		}
		
		context.renderer ().renderTemplate ( "login.html" );
	}

	public static void reset ( DrumlinRequestContext context, String tag ) throws IOException
	{
		context.renderer ().put ( "tag", tag );
		context.renderer ().renderTemplate ( "reset.html" );
	}

	public static void finishReset ( DrumlinRequestContext context, String tag ) throws IOException
	{
		final DrumlinAccountMgr<exampleUser,exampleAccount> am = ((loginServlet)(context.getServlet ())).getAccountMgr ();
		final DrumlinFormPostWrapper wrapper = new DrumlinFormPostWrapper ( context.request () );
		try
		{
			if ( null != am.completePasswordReset ( tag, wrapper.getValue ( "password" ) ) )
			{
				context.renderer ().put ( "lastResult", "password changed" );
			}
			else
			{
				context.renderer ().put ( "lastResult", "password NOT changed" );
			}
		}
		catch ( DrumlinAccountsException e )
		{
			context.renderer ().put ( "lastResult", "problem: " + e.getMessage() );
		}
	
		context.renderer ().renderTemplate ( "login.html" );
	}
}
