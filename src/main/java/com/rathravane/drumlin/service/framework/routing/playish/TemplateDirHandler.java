/*
 *	Copyright 2006-2013, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.routing.playish;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;

public class TemplateDirHandler implements DrumlinPlayishRouteHandler
{
	public TemplateDirHandler ( String dirInfo )
	{
	}

	@Override
	public void handle ( DrumlinRequestContext context, List<String> args )
		throws IOException,
			IllegalArgumentException,
			IllegalAccessException,
			InvocationTargetException
	{
		final String path = context.request ().getPathInContext ();
		if ( path != null && path.length() > 0 )
		{
			final String file = path.substring ( 1 );
			context.renderer ().renderTemplate ( file );
		}
		else
		{
			throw new IOException ( "Couldn't render path." );
		}
	}

	@Override
	public boolean actionMatches ( String fullPath )
	{
		return false;
	}
}
