/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.service.framework.context;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

import com.rathravane.drumlin.service.framework.DrumlinConnection;
import com.rathravane.drumlin.service.framework.routing.DrumlinRequestRouter;
import com.rathravane.drumlin.service.standards.HttpMethods;
import com.rathravane.drumlin.service.standards.MimeTypes;

class StdResponse implements DrumlinResponse
{
	public StdResponse ( HttpServletRequest req, HttpServletResponse r, DrumlinRequestRouter rr )
	{
		fRequest = req;
		fResponseEntityAllowed = !(req.getMethod ().equalsIgnoreCase ( HttpMethods.HEAD ));
		fResponse = r;
		fRouter = rr;
		writeHeader ( "X-Rathravane", "~ software is craft ~" );
	}

	@Override
	public void sendErrorAndBody ( int err, String content, String mimeType )
	{
		try
		{
			setStatus ( err );
			getStreamForTextResponse ( mimeType ).println ( content );
		}
		catch ( IOException e )
		{
			log.warn ( "Error sending error response: " + e.getMessage () );
		}
	}

	@Override
	public void sendError ( int err, String msg )
	{
		sendStatusAndMessage ( err, msg );
	}
	
	@Override
	public void sendStatusAndMessage ( int status, String msg )
	{
		try
		{
			fResponse.sendError ( status, msg );
		}
		catch ( IOException e )
		{
			log.error ( "Error sending response: " + e.getMessage () );
		}
	}

	@Override
	public DrumlinResponse setStatus ( int code )
	{
		fResponse.setStatus ( code );
		return this;
	}

	@Override
	public int getStatusCode ()
	{
		return fResponse.getStatus ();
	}

	@Override
	public DrumlinResponse setContentType ( String mimeType )
	{
		fResponse.setContentType ( mimeType );
		return this;
	}

	@Override
	public DrumlinResponse send ( String content ) throws IOException
	{
		final PrintWriter pw = new PrintWriter ( fResponse.getWriter () );
		pw.print ( content );
		pw.close ();
		return this;
	}

	@Override
	public DrumlinResponse writeHeader ( String headerName, String headerValue )
	{
		writeHeader ( headerName, headerValue, false );
		return this;
	}

	@Override
	public DrumlinResponse writeHeader ( String headerName, String headerValue, boolean overwrite )
	{
		if ( overwrite )
		{
			fResponse.setHeader ( headerName, headerValue );
		}
		else
		{
			fResponse.addHeader ( headerName, headerValue );
		}
		return this;
	}

	@Override
	public OutputStream getStreamForBinaryResponse () throws IOException
	{
		return getStreamForBinaryResponse ( MimeTypes.kAppGenericBinary );
	}

	@Override
	public OutputStream getStreamForBinaryResponse ( String contentType ) throws IOException
	{
		fResponse.setContentType ( contentType );

		OutputStream os ;
		if ( fResponseEntityAllowed )
		{
			os = fResponse.getOutputStream ();
		}
		else
		{
			os = new NullStream ();
		}
		return os;
	}

	@Override
	public PrintWriter getStreamForTextResponse ()
		throws IOException
	{
		return getStreamForTextResponse ( "text/html" );
	}

	@Override
	public PrintWriter getStreamForTextResponse ( String contentType ) throws IOException
	{
		return getStreamForTextResponse ( contentType, "UTF-8" );
	}

	@Override
	public PrintWriter getStreamForTextResponse ( String contentType, String encoding ) throws IOException
	{
		fResponse.setContentType ( contentType );
		fResponse.setCharacterEncoding ( encoding );

		PrintWriter pw ;
		if ( fResponseEntityAllowed )
		{
			pw = fResponse.getWriter ();
		}
		else
		{
			pw = new PrintWriter ( new NullWriter () );
		}
		return pw;
	}

	@Override
	public void redirect ( String url )
	{
		redirectExactly ( DrumlinRequestContext.servletPathToFullPath ( url, fRequest ) );
	}

	@Override
	public void redirect ( Class<?> cls, String method, DrumlinConnection forSession )
	{
		redirect ( cls, method, new HashMap<String, Object> (), forSession );
	}

	@Override
	public void redirect ( Class<?> cls, String method, Map<String, Object> args, DrumlinConnection forSession )
	{
		String localUrl = fRouter.reverseRoute ( cls, method, args, forSession );
		if ( localUrl == null )
		{
			log.error ( "No reverse route for " + cls.getName () + "::" + method + " with " + (args == null ? 0 : args.size () ) + " args." );
			localUrl = "/";
		}
		redirect ( localUrl );
	}

	@Override
	public void redirectExactly ( String url )
	{
		try
		{
			fResponse.sendRedirect ( url );
		}
		catch ( IOException e )
		{
			log.error ( "Error sending redirect: " + e.getMessage () );
		}
	}

	private final HttpServletRequest fRequest;
	private final boolean fResponseEntityAllowed;
	private final HttpServletResponse fResponse;
	private final DrumlinRequestRouter fRouter;

	private static org.slf4j.Logger log = LoggerFactory.getLogger ( StdResponse.class );

	private static class NullWriter extends Writer
	{
        /**
         *
         * @param cbuf
         * @param off
         * @param len
         */
		@Override
		public void write ( char[] cbuf, int off, int len )
		{
		}

		@Override
		public void flush ()
		{
		}

		@Override
		public void close ()
		{
		}
	}

	private static class NullStream extends OutputStream
	{
		@Override
		public void write ( int b ) {}
	}
}
