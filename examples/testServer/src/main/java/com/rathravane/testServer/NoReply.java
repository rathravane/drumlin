package com.rathravane.testServer;

import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.standards.HttpStatusCodes;

public class NoReply
{
	public static void get ( DrumlinRequestContext ctx )
	{
		try
		{
			Thread.sleep ( 24 * 60 * 60 * 1000 );
		}
		catch ( InterruptedException e )
		{
			ctx.response ().sendError ( HttpStatusCodes.k406_notAcceptable, "test end point that doesn't reply" );
		}
	}
}
