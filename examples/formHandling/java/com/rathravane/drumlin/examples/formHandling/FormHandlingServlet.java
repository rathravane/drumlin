/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */

package com.rathravane.drumlin.examples.formHandling;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;

import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper;
import com.rathravane.drumlin.app.htmlForms.mime.DrumlinMimePart;
import com.rathravane.drumlin.service.framework.DrumlinServlet;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.framework.context.DrumlinRequest;
import com.rathravane.drumlin.service.framework.routing.DrumlinSimpleRouteHandler;
import com.rathravane.drumlin.service.standards.HttpMethods;
import com.rathravane.drumlin.service.standards.HttpStatusCodes;

public class FormHandlingServlet extends DrumlinServlet
{
	@Override
	protected void servletSetup () throws ServletException
	{
		getRequestRouter().addRouteSource ( 
			new DrumlinSimpleRouteHandler ()
			{
				@Override
				public void run ( DrumlinRequestContext ctx ) throws IOException
				{
					String msg = "";

					final DrumlinRequest req = ctx.request ();
					if ( req.getMethod ().equalsIgnoreCase ( HttpMethods.POST ))
					{
						final StringBuffer lastPost = new StringBuffer ();

						final DrumlinFormPostWrapper wrap = new DrumlinFormPostWrapper ( req );
						try
						{
							final String first = wrap.getValue ( "first" );
							final String second = wrap.getValue ( "second" );
							lastPost.append ( "first: " + first + "<br/>second: " + second + "<br/>\n" );

							final DrumlinMimePart mp = wrap.getStream ( "f" );
							if ( mp != null )
							{
								final InputStream is = mp.openStream ();
								
								final byte[] b = new byte[1024];
								int len = -1;
								int total = 0;
								while ( (len=is.read ( b )) >= 0 )
								{
									total += len;
								}
								is.close ();

								lastPost.append ( "a file of length: " + total + " bytes<br/>\n" );
							}
						}
						finally
						{
							wrap.close ();
						}
						msg = lastPost.toString ();
					}

					ctx.response ().
						setStatus ( HttpStatusCodes.k200_ok ).
						getStreamForTextResponse ().println ( kForm.replace ( "%MESSAGE%", msg ) );
				}
			} );
	}

	private static final long serialVersionUID = 1L;

	private static final String kForm =
		"<html><head><title>Drumlin</title>\n<style>label { min-width:10em; display:inline-block; text-align: right; margin-right:1em;}  form{margin-top:3em;}</style></head>\n" +
		"<body><form method=\"POST\" enctype=\"multipart/form-data\">" +
			"<label>first</label><input type=\"text\" name=\"first\"><br/>\n" +
			"<label>second</label><input type=\"text\" name=\"second\"><br/>\n" +
			"<label>third</label><input type=\"file\" name=\"f\" size=\"50\">\n" +
			"<input type=\"submit\" value=\"go\">\n" +
		"</form><br/>\n" +
		"%MESSAGE% <br/>\n</body>" +
		"</html>"
	;
}
