/*
 *	Copyright 2006-2013, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.examples.accounts;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;

import com.rathravane.drumlin.app.accounts.DrumlinAccountMgr;
import com.rathravane.drumlin.app.accounts.DrumlinAccountsException;
import com.rathravane.drumlin.app.accounts.DrumlinApiKey;
import com.rathravane.till.console.cmdLinePrefs;
import com.rathravane.till.console.rrCmdLineParser;
import com.rathravane.till.console.rrConsole;
import com.rathravane.till.console.shell.consoleLooper;
import com.rathravane.till.console.shell.consoleLooper.inResult;
import com.rathravane.till.console.shell.simpleCommand;
import com.rathravane.till.console.shell.stdCommandList;
import com.rathravane.till.data.sha1HmacSigner;
import com.rathravane.till.nv.rrNvReadable;
import com.rathravane.till.nv.rrNvReadable.missingReqdSetting;
import com.rathravane.till.store.rrJsonObjectFile;

/**
 * This is an example of using the account manager. It uses a Till JSON object file for persistence.
 * For simplicity, account and user IDs are just the block locations in the file. This would be a
 * bad idea for a production system, as a bogus account/user ID would allow a read or write starting from
 * anywhere in the file.
 *  
 * @author peter
 *
 */
public class exampleAccountMgrConsole extends rrConsole
{
	public exampleAccountMgrConsole ()
	{
		fMgr = null;
		fFile = null;
		fJsonFile = null;
	}

	@Override
	protected void setupOptions ( rrCmdLineParser p )
	{
		p.registerOptionWithValue ( "file", "f", null, null );
	}
	
	@Override
	protected looper init ( rrNvReadable p, cmdLinePrefs cmdLine ) throws rrNvReadable.missingReqdSetting, rrNvReadable.invalidSettingValue, startupFailureException
	{
		try
		{
			final String filename = p.getString ( "file" );
			final File f = new File ( filename );
			if ( !f.exists () )
			{
				exampleAcctPersistence.initialize ( f, null );
			}

			fJsonFile = new rrJsonObjectFile ( f, true, null ); 
			fFile = new exampleAcctPersistence ( fJsonFile );
			fMgr = new DrumlinAccountMgr<exampleUser,exampleAccount> ( fFile );

			final stdCommandList cl = new stdCommandList ();

			// create an account
			cl.registerCommand ( new simpleCommand ( "createAccount" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					super.setupParser ( clp );
					clp.requireOneFileArgument ();
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String acctId = p.getFileArguments().elementAt ( 0 );
						fMgr.createAccount ( acctId );
						outTo.println ( "created [" + acctId + "]." );
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "createUser" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					super.setupParser ( clp );
					clp.requireFileArguments ( 2 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String acctId = p.getFileArguments().elementAt ( 0 );
						final String userId = p.getFileArguments().elementAt ( 1 );

						if ( null != fMgr.createUser ( acctId, "" + userId, null ) )
						{
							outTo.println ( "created [" + userId + "]." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "enableUser" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					super.setupParser ( clp );
					clp.requireFileArguments ( 1 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String userId = p.getFileArguments().elementAt ( 0 );

						fMgr.enableUser ( userId, true );
						outTo.println ( "user [" + userId + "] enabled." );
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "enableAccount" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					super.setupParser ( clp );
					clp.requireFileArguments ( 1 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String acctId = p.getFileArguments().elementAt ( 0 );

						fMgr.enableAccount ( acctId, true );
						outTo.println ( "account [" + acctId + "] enabled." );
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "auth" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 2, 2 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String userId = p.getFileArguments().elementAt ( 0 );
						final String pwd = p.getFileArguments().elementAt ( 1 );

						final exampleUser u = fMgr.authenticateUserWithPassword ( userId, pwd );
						if ( u != null )
						{
							outTo.println ( "Logged in." );
						}
						else
						{
							outTo.println ( "Login failed." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "passwdReset" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 1 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String userId = p.getFileArguments().elementAt ( 0 );
						final String tag = fMgr.requestPasswordReset ( userId, 60, "some nonsense input for tag creation" );
						outTo.println ( "Tag: [" + tag + "]." );
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "finishPasswdReset" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 2 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String tag = p.getFileArguments().elementAt ( 0 );
						final String passwd = p.getFileArguments().elementAt ( 1 );

						if ( null != fMgr.completePasswordReset ( tag, passwd ) )
						{
							outTo.println ( "Done." );
						}
						else
						{
							outTo.println ( "Not completed." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "createUserApiKey" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 1, 1 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String userId = p.getFileArguments().elementAt ( 0 );

						final exampleUser u = fMgr.loadUser ( userId );
						if ( u != null )
						{
							final DrumlinApiKey apiKey = fMgr.createUserApiKey ( userId );
							if ( apiKey != null )
							{
								outTo.println ( "   key: " + apiKey.getKey () );
								outTo.println ( "secret: " + apiKey.getSecret () );
							}
							else
							{
								outTo.println ( "API create call failed." );
							}
						}
						else
						{
							outTo.println ( "no such user." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "createAccountApiKey" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 1, 1 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String acctId = p.getFileArguments().elementAt ( 0 );

						final exampleAccount a = fMgr.loadAccount ( acctId );
						if ( a != null )
						{
							final DrumlinApiKey apiKey = fMgr.createAcctApiKey ( acctId );
							if ( apiKey != null )
							{
								outTo.println ( "   key: " + apiKey.getKey () );
								outTo.println ( "secret: " + apiKey.getSecret () );
							}
							else
							{
								outTo.println ( "API create call failed." );
							}
						}
						else
						{
							outTo.println ( "no such user." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			cl.registerCommand ( new simpleCommand ( "useApiKey" )
			{
				@Override
				protected void setupParser ( rrCmdLineParser clp )
				{
					clp.requireFileArguments ( 2, 2 );
				}

				@Override
				protected inResult execute ( HashMap<String, Object> workspace, cmdLinePrefs p, PrintStream outTo ) throws usageException, missingReqdSetting
				{
					try
					{
						final String apiKey = p.getFileArguments().elementAt ( 0 );
						final String apiSecret = p.getFileArguments().elementAt ( 1 );

						// first construct a signed message
						final String msg = "This is a message to sign.";
						final String signed = sha1HmacSigner.sign ( msg, apiSecret );
						outTo.println ( "Signing [" + msg + "] with secret key, result is [" + signed + "]." );

						// authenticate
						final DrumlinApiKey key = fMgr.authenticateViaApiKey ( apiKey, msg, signed );
						if ( key != null )
						{
							DrumlinApiKey.type t = key.getAccessType ();
							if ( t.equals ( DrumlinApiKey.type.ACCOUNT ) )
							{
								outTo.println ( "API key resulted in authentication for account " + key.getAccount () );
							}
							else
							{
								outTo.println ( "API key resulted in authentication for user " + key.getUser () );
							}
						}
						else
						{
							outTo.println ( "API key not recognized." );
						}
					}
					catch ( DrumlinAccountsException e )
					{
						outTo.println ( e.getMessage () );
					}
					return inResult.kReady;
				}
			} );

			return new consoleLooper (
				new String[] { "Example Account Manager", "Drumlin Project by Rathravane (www.rathravane.com)" },
				"am> ", ". ", cl );
		}
		catch ( IOException e )
		{
			throw new startupFailureException ( e );
		}
	}

	public static void main ( String[] args ) throws Exception
	{
		final exampleAccountMgrConsole c = new exampleAccountMgrConsole ();
		c.runFromMain ( args );
	}

	private DrumlinAccountMgr<exampleUser,exampleAccount> fMgr;
	private exampleAcctPersistence fFile;
	private rrJsonObjectFile fJsonFile;
}
