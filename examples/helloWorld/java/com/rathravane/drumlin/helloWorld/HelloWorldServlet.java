/*
 *	Copyright 2006-2013, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.helloWorld;

import java.io.IOException;

import com.rathravane.drumlin.service.framework.DrumlinServlet;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.framework.routing.DrumlinSimpleRouteHandler;
import com.rathravane.drumlin.service.standards.HttpStatusCodes;

public class HelloWorldServlet extends DrumlinServlet
{
	@Override
	protected void servletSetup ()
	{
		// a Drumlin servlet has a "request router" that routes incoming HTTP
		// requests. Here, we add a route source that sends a simple reply for any request.

		getRequestRouter ().addRouteSource ( 
			new DrumlinSimpleRouteHandler ()
			{
				@Override
				public void run ( DrumlinRequestContext ctx ) throws IOException
				{
					ctx.response ().
						setStatus ( HttpStatusCodes.k200_ok ).
						getStreamForTextResponse ().
							println ( "<html><head><title>Drumlin</title></head><body>Hello, world!</body></html>" );
				}
			} );
	}

	private static final long serialVersionUID = 1L;
}
