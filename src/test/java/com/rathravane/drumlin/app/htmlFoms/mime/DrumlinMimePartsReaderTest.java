package com.rathravane.drumlin.app.htmlFoms.mime;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import com.rathravane.drumlin.app.htmlForms.DrumlinFormPostWrapper.inMemoryFormDataPart;
import com.rathravane.drumlin.app.htmlForms.mime.DrumlinMimePart;
import com.rathravane.drumlin.app.htmlForms.mime.DrumlinMimePartFactory;
import com.rathravane.drumlin.app.htmlForms.mime.DrumlinMimePartsReader;
import com.rathravane.till.collections.rrMultiMap;

public class DrumlinMimePartsReaderTest extends TestCase
{
	@Test
	public void testSimpleMultipartStream () throws IOException
	{
		final DrumlinMimePartsReader reader = new DrumlinMimePartsReader ( "---foobar", new testStorage () );
		final String input = "\n" +
				"-----foobar\r\n" +
				"Content-Disposition: form-data; name=\"caption\"\r\n" +
				"\r\n" + 
				"test\r\n" + 
				"-----foobar\r\n" +
				"Content-Disposition: form-data; name=\"image\"; filename=\"myphoto.jpg\"\r\n" +
				"Content-Type: image/jpeg\r\n" + 
				"\r\n" + 
				"binary-data\r\n" + 
				"-----foobar--\r\n";

		final InputStream is = new ByteArrayInputStream ( input.getBytes () ); 
		reader.read ( is );

		final List<DrumlinMimePart> parts = reader.getParts ();
		assertEquals ( 2, parts.size() );

		final DrumlinMimePart p1 = parts.remove ( 0 );
		assertNotNull ( p1 );

		final DrumlinMimePart p2 = parts.remove ( 0 );
		assertNotNull ( p2 );

		assertEquals ( "test", p1.getAsString () );
	}

	static class testStorage implements DrumlinMimePartFactory
	{
		@Override
		public DrumlinMimePart createPart ( rrMultiMap<String, String> partHeaders ) throws IOException
		{
			final String contentDisp = partHeaders.getFirst ( "content-disposition" );
			if ( contentDisp != null && contentDisp.contains ( "filename=\"" ) )
			{
				// would be a file
				return new inMemoryFormDataPart ( partHeaders.getFirst ( "content-type" ), contentDisp );
			}
			else
			{
				return new inMemoryFormDataPart ( partHeaders.getFirst ( "content-type" ), contentDisp );
			}
		}
	}
}
