/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.app.htmlForms;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

/**
 * A request reader. Is this in use??
 * @author peter@rathravane.com
 *
 */
@Deprecated
public class DrumlinRequestReader
{
	public static class invalidRequest extends Exception
	{
		public invalidRequest ( String msg )
		{
			super ( msg );
		}
		private static final long serialVersionUID = 1L;
	}

	public static class badInputRequest extends invalidRequest
	{
		public badInputRequest ( String key, String msg )
		{
			super ( msg );
			fKey = key;
			fMsg = msg;
		}
		public final String fKey;
		public final String fMsg;
		private static final long serialVersionUID = 1L;
	}

	public DrumlinRequestReader ()
	{
		fFieldMap = new HashMap<>();
	}

	public void registerArgument ( String key, boolean reqd, boolean notEmpty, String defVal, String[] limitTo )
	{
		final fieldInfo fi = new fieldInfo ();
		fi.fDefVal = defVal;
		fi.fReqd = reqd;
		fi.fNotEmpty = notEmpty;
		fi.fValueLimitedTo = limitTo;

		fFieldMap.put ( key, fi );
	}

	public Map<String,String> read ( HttpServletRequest req ) throws invalidRequest
	{
		final HashMap<String,String> valueMap = new HashMap<>();

		// start with required defaults
		for ( final Entry<String, fieldInfo> e : fFieldMap.entrySet () )
		{
			final String key = e.getKey ();
			final fieldInfo fi = e.getValue ();
			if ( fi.fReqd && fi.fDefVal != null )
			{
				valueMap.put ( key, fi.fDefVal );
			}
		}

		// read the input
		final Map<?,?> params = req.getParameterMap ();
		for ( Entry<?, ?> e : params.entrySet () )
		{
			final String p = e.getKey().toString ();
			final String[] vArray = (String[]) e.getValue();
			final String v = vArray.length > 0 ? vArray[0] : null;

			final fieldInfo fi = fFieldMap.get ( p );
			if ( fi != null )
			{
				if ( fi.fValueLimitedTo != null )
				{
					boolean found = false;
					for ( int i=0; !found && i<fi.fValueLimitedTo.length; i++ )
					{
						found = fi.fValueLimitedTo[i].equals ( v );
					}
					if ( !found )
					{
						final StringBuilder sb = new StringBuilder ();
						sb.append ( "Invalid value: [" ).append ( v ).append ( "]; it must be one of: " );
						for ( String s : fi.fValueLimitedTo )
						{
							sb.append ( s );
							sb.append ( " " );
						}
						throw new badInputRequest ( p, sb.toString () );
					}
				}
				valueMap.put ( p, v );
			}
		}

		// validate the settings
		for ( final Entry<String, fieldInfo> e : fFieldMap.entrySet () )
		{
			final String key = e.getKey ();
			final fieldInfo fi = e.getValue ();
			if ( fi.fReqd )
			{
				final String val = valueMap.get ( key );
				if ( val == null || ( fi.fNotEmpty && val.length() == 0 ) )
				{
					throw new badInputRequest ( key, "Please provide a value!" );
				}
			}
			else if ( fi.fNotEmpty )
			{
				final String val = valueMap.get ( key );
				if ( val != null && val.length() == 0 )
				{
					throw new badInputRequest ( key, "Please provide a value!" );
				}
			}
		}

		return valueMap;
	}

	private class fieldInfo
	{
		public fieldInfo ()
		{
			fDefVal = null;
			fReqd = false;
			fValueLimitedTo = null;
		}

		public String fDefVal;
		public boolean fReqd;
		public boolean fNotEmpty;
		public String[] fValueLimitedTo;
	}
	private HashMap<String,fieldInfo> fFieldMap;
}
