/*
 *	Copyright 2006-2012, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.drumlin.playish.handlers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.standards.HttpStatusCodes;

public class DateReport
{
	public static void get ( DrumlinRequestContext context ) throws IOException
	{
		final String d = new SimpleDateFormat ( "yyyy-MM-dd" ).format ( new Date () );
		context.response ().
			setStatus ( HttpStatusCodes.k200_ok ).
			getStreamForTextResponse ().println ( "<html><head><title>Drumlin</title></head><body>" + d + "</body></html>" );
	}
}
