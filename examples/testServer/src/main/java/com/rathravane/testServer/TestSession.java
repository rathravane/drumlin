package com.rathravane.testServer;

import java.util.HashMap;
import java.util.LinkedList;

import javax.servlet.ServletException;

import com.rathravane.drumlin.service.framework.DrumlinConnection;
import com.rathravane.drumlin.service.framework.DrumlinConnectionContext;
import com.rathravane.drumlin.service.framework.DrumlinServlet;

public class TestSession implements DrumlinConnection
{
	public TestSession ()
	{
		fMsgs = new LinkedList<Message> ();
		fLoginDestination = null;
	}

	@Override
	public void onSessionCreate ( DrumlinServlet ws, DrumlinConnectionContext dcc ) throws ServletException
	{
	}

	@Override
	public void onSessionClose ()
	{
	}

	@Override
	public void noteActivity ()
	{
	}

	@Override
	public void buildTemplateContext ( HashMap<String, Object> context )
	{
		final LinkedList<Message> msgs = new LinkedList<Message> ( fMsgs );
		context.put ( "messages", msgs );
		fMsgs.clear();
	}
	
	private final LinkedList<Message> fMsgs;
	private String fLoginDestination;

	public enum MessageLevel
	{
		WARNING,
		INFO,
	};

	public static class Message
	{
		public Message ( MessageLevel ml, String msg )
		{
			fLevel = ml;
			fMsg = msg;
		}
		public String getLevel () { return fLevel.toString (); }
		public String getMessage () { return fMsg; }
		public final MessageLevel fLevel;
		public final String fMsg;
	}

	public void putMessage ( MessageLevel ml, String msg )
	{
		fMsgs.add ( new Message ( ml, msg ) );
	}

	public String getLoginDestination ()
	{
		try
		{
			return fLoginDestination;
		}
		finally
		{
			fLoginDestination = null;
		}
	}

	public void putLoginDestination ( String reverseRoute )
	{
		fLoginDestination = reverseRoute;
	}
}
