package com.rathravane.drumlin.app.userAgents.devices.android;

import com.rathravane.drumlin.app.userAgents.devices.genericDevice;
import com.rathravane.drumlin.app.userAgents.devices.screenInfo;
import com.rathravane.drumlin.app.userAgents.devices.unknownFixedScreen;

public class androidDevice extends genericDevice
{
	public androidDevice ()
	{
		super ( new unknownFixedScreen (), true );
	}

	public androidDevice ( screenInfo si )
	{
		super ( si, true );
	}

	@Override
	public String getOsName ()
	{
		return "Android";
	}
}
