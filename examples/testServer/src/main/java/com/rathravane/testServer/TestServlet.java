package com.rathravane.testServer;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;

import org.json.JSONException;

import com.rathravane.drumlin.service.framework.DrumlinErrorHandler;
import com.rathravane.drumlin.service.framework.DrumlinServlet;
import com.rathravane.drumlin.service.framework.context.DrumlinRequestContext;
import com.rathravane.drumlin.service.framework.routing.DrumlinRequestRouter;
import com.rathravane.drumlin.service.framework.routing.playish.DrumlinPlayishRoutingFileSource;
import com.rathravane.drumlin.service.standards.HttpStatusCodes;
import com.rathravane.drumlin.service.standards.MimeTypes;
import com.rathravane.till.logging.rrLogSetup;
import com.rathravane.till.nv.rrNvReadable;

public class TestServlet extends DrumlinServlet
{
	public TestServlet ()
	{
		this ( null );
	}

	public TestServlet ( rrNvReadable prefs )
	{
		super ( prefs, null, sessionLifeCycle.FULL_SESSION );
	}

	@Override
	public TestSession createSession () throws rrNvReadable.missingReqdSetting
	{
		return new TestSession ();
	}

	@Override
	protected void servletSetup () throws rrNvReadable.missingReqdSetting, rrNvReadable.invalidSettingValue, ServletException
	{
		try
		{
			final rrNvReadable p = super.getSettings ();

			// setup request routing
			setupRouter ( p );

			log.info ( "The server is ready." );
		}
		catch ( IOException e )
		{
			throw new ServletException ( e );
		}
		catch ( NumberFormatException e )
		{
			throw new ServletException ( e );
		}
	}

	@Override
	protected void servletShutdown ()
	{
	}

	private void setupRouter ( rrNvReadable p ) throws IOException
	{
		final DrumlinRequestRouter rr = super.getRequestRouter ();

		rr.setHandlerForException ( DrumlinRequestRouter.noMatchingRoute.class,
			new DrumlinErrorHandler ()
			{
				@Override
				public void handle ( DrumlinRequestContext ctx, Throwable cause )
				{
					ctx.renderer ().renderTemplate ( "templates/notFound.html" );
					ctx.response ().setStatus ( HttpStatusCodes.k404_notFound );
				}
			} );

		rr.setHandlerForException ( JSONException.class,
			new DrumlinErrorHandler ()
			{
				@Override
				public void handle ( DrumlinRequestContext ctx, Throwable cause )
				{
					ctx.response ().sendErrorAndBody ( HttpStatusCodes.k400_badRequest, "Bad request.", MimeTypes.kPlainText );
				}
			} );

		// setup admin routes
		URL url = findStream ( "routes/testServer.conf" );
		rr.addRouteSource ( new DrumlinPlayishRoutingFileSource ( url ) );
	}

	private static final long serialVersionUID = 1L;
	private static final org.slf4j.Logger log = rrLogSetup.getLog ( TestServlet.class );
}
