package com.rathravane.drumlin.examples.accounts;

import com.rathravane.drumlin.service.standalone.DrumlinStandaloneServer;
import com.rathravane.till.nv.rrNvWriteable;

public class loginServer extends DrumlinStandaloneServer
{
	@Override
	protected void setupDefaults ( rrNvWriteable pt )
	{
		super.setupDefaults ( pt );

		pt.set ( kSetting_WebRoot, "./examples/accounts/war.accts" );
		pt.set ( kSetting_PortNumber, "8080" );
	}

	public static void main ( String[] args )
	{
		try
		{
			System.out.println ( "accounts example" );

			new loginServer ().runFromMain ( args );
		}
		catch ( Exception e )
		{
			System.err.println ( e.getMessage () );
			e.printStackTrace ( System.err );
		}
	}
}
