package com.rathravane.drumlin.app.userAgents.devices.ios;

import com.rathravane.drumlin.app.userAgents.devices.screenInfo;

public class iPhone4 extends iPhone
{
	public iPhone4 ()
	{
		super ( new screenInfo ( 640, 960, 326 ) );
	}
}
